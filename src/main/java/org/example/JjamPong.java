package org.example;

public class JjamPong {
    String title;

    String mainMaterial;

    public JjamPong(String title, String mainMaterial) {
        this.title = title;
        this.mainMaterial = mainMaterial;
    }

    public String makeJjamPong() {
        return "맛있는 " + mainMaterial + " 넣은 " + title + "짬뽕";
    }
}
