package org.example;

public class MeatCheckRoom {
    String meatOrigin;

    public MeatCheckRoom(String meatOrigin) {
        this.meatOrigin = meatOrigin;
    }

    public boolean checkMeat() {
        if (this.meatOrigin.equals("국내산")) {
            return true;
        } else {
            return false;
        }
    }
}
