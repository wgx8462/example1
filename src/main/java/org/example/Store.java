package org.example;

public class Store {
    String title;

    public Store(String title) {
        this.title = title;
    }

    public void infoWoman() {
        switch (this.title) {
            case "명품":
                System.out.println("1층 입니다.");
                break;
            case "캐쥬얼":
                System.out.println("3층 입니다.");
                break;
            case "정장":
                System.out.println("4층 입니다.");
                break;
            default:
                System.out.println("우리 백화점에 없어요 나가세요!!");
                break;
        }
    }
}
